---
title: Commandes Pandoc
---

## Commandes Pandoc

### Vérifier que pandoc est installé (et quelle version)

Commande :

```bash
pandoc --version
```

Réponse : 

```
pandoc.exe 2.17.1.1
Compiled with pandoc-types 1.22.1, texmath 0.12.4, skylighting 0.12.2, citeproc 0.6.0.1, ipynb 0.2
User data directory: C:\Users\jrabaud001\AppData\Roaming\pandoc
Copyright (C) 2006-2022 John MacFarlane. Web:  https://pandoc.org
This is free software; see the source for copying conditions. There is no warranty, not even for merchantability or fitness for a particular purpose.
```

### Commandes dans `Article` 

#### Word avec bibliographie et sommaire

```sh
pandoc articleFauchie.md --toc --toc-depth=3 -C --bibliography=data/Article-Fauchie.json --csl=data/iso690-author-date-fr.csl -o sorties/articlesFauchie_ISO690.docx
```

-C

#### Word avec un modèle word et autre style csl

```sh
pandoc articleFauchie.md --toc --toc-depth=3 --reference-doc=data/styleBraud.docx -C --bibliography=data/Article-Fauchie.json --csl=data/transversalites-Braud.csl -o sorties/articlesFauchie_styleBraud.docx
```

`--reference-doc=FILE`


#### html

```sh
pandoc articleFauchie.md -s --self-contained --toc --toc-depth=3 -C --bibliography=data/Article-Fauchie.json --csl=data/iso690-author-date-fr.csl -o sorties/articleHTMLsimple.html
```

`-s` `--self-contained`

#### html avec css (bleu biblio à droite)

```sh
pandoc articleFauchie.md -s --self-contained --include-in-header=data/bleu.css --toc --toc-depth=3 -C --bibliography=data/Article-Fauchie.json --csl=data/iso690-author-date-fr.csl -o sorties/articleHTML_bleu.html
```

`--include-in-header=FILE`

#### html avec css (orange, notes à droite)

```sh
pandoc articleFauchie.md -s --self-contained --reference-location=block --include-in-header=data/orange.css --toc --toc-depth=3 -C --bibliography=data/Article-Fauchie.json --csl=data/iso690-author-date-fr.csl -o sorties/articleHTML_orange.html
```

`--reference-location=block`

#### Pdf avec liste des figures

```sh
pandoc articleFauchie.md --pdf-engine=xelatex --metadata notes-after-punctuation=false --toc --toc-depth=3 --reference-doc=data/styleBraud.docx -C --bibliography=data/Article-Fauchie.json --csl=data/transversalites-Braud.csl -o sorties/articlePDF.pdf
```

Ajouter une ligne dans le YAML : `lof: true`

`--metadata notes-after-punctuation=false`



### Options d'écriture à tester

#### `--section-divs`

Wrap sections in `<section>` tags (or `<div>` tags for `html4`), and attach identifiers to the enclosing `<section>` (or `<div>`) rather than the heading itself. See [Heading identifiers](https://pandoc.org/MANUAL.html#heading-identifiers), below.


#### `--top-level-division=default`|`section`|`chapter`|`part`

Treat top-level headings as the given division type in LaTeX, ConTeXt, DocBook, and TEI output. The hierarchy order is part, chapter, then section; all headings are shifted such that the top-level heading becomes the specified type. The default behavior is to determine the best division type via heuristics: unless other conditions apply, `section` is chosen. When the `documentclass` variable is set to `report`, `book`, or `memoir` (unless the `article` option is specified), `chapter` is implied as the setting for this option. If `beamer` is the output format, specifying either `chapter` or `part` will cause top-level headings to become `\part{..}`, while second-level headings remain as their default type.



