---
title: "Extrait du Manuel Pandoc : templates et modèles de document"
---


## Commandes exécutées dans le dossier `templates-pandoc` :
- Templates :
  - HTML5 : `pandoc -o template_html5.html -D html5`
  - LaTeX : `pandoc -o template.latex -D latex`
  - reveal.js (slides html) : `pandoc -o template_revealjs.html -D revealjs`
- Modèles : 
  - Word : `pandoc -o reference.docx --print-default-data-file reference.docx`
  - LibreOffice : `pandoc -o reference.odt --print-default-data-file reference.odt`
  - PowerPoint : `pandoc -o reference.pptx --print-default-data-file reference.pptx`


## Templates 

- [Dépôt officiel](https://github.com/jgm/pandoc-templates) 
- [Section du Manuel Pandoc](https://pandoc.org/MANUAL.html#templates)
- pour afficher une template dans le terminal :
  - `pandoc -D *FORMAT*`
    - |`pandoc -D html5` |`pandoc -D html4` |`pandoc -D epub3`
    - |`pandoc -D rtf`
    - |`pandoc -D jats_articleauthoring` |`pandoc -D jats_archiving` |`pandoc -D jats_publishing`
    - |`pandoc -D tei`
    - |`pandoc -D latex` (pour les **pdf**)
    - *etc*.

## Modèles de documents (`.docx`, `.odt` et `.pptx`)

> ## Docx
>
> For best results, the reference docx should be a modified version of a docx file produced using pandoc. The contents of the reference docx are ignored, but its stylesheets and document properties (including margins, page size, header, and footer) are used in the new docx. If no reference docx is specified on the command line, pandoc will look for a file `reference.docx` in the user data directory (see `--data-dir`). If this is not found either, sensible defaults will be used.
>
> To produce a custom `reference.docx`, first get a copy of the default `reference.docx`: 
>
> ```
> pandoc -o custom-reference.docx --print-default-data-file reference.docx 
> ```
>
> Then open `custom-reference.docx` in Word, modify the styles as you wish, and save the file. For best results, do not make changes to this file other than modifying the styles used by pandoc:
>
> - Paragraph styles:
>   - Normal
>   - Body Text
>   - First Paragraph
>   - Compact
>   - Title
>   - Subtitle
>   - Author
>   - Date
>   - Abstract
>   - Bibliography
>   - Heading 1
>   - Heading 2
>   - Heading 3
>   - Heading 4
>   - Heading 5
>   - Heading 6
>   - Heading 7
>   - Heading 8
>   - Heading 9
>   - Block Text
>   - Footnote Text
>   - Definition Term
>   - Definition
>   - Caption
>   - Table Caption
>   - Image Caption
>   - Figure
>   - Captioned Figure
>   - TOC Heading
>
> - Character styles:
>   - Default Paragraph Font
>   - Body Text Char
>   - Verbatim Char
>   - Footnote Reference
>   - Hyperlink
>   - Section Number
>
> - Table style:
>   - Table
>
> ## ODT
>
> For best results, the reference ODT should be a modified version of an ODT produced using pandoc. The contents of the reference ODT are ignored, but its stylesheets are used in the new ODT. If no reference ODT is specified on the command line, pandoc will look for a file reference.odt in the user data directory (see `--data-dir`). If this is not found either, sensible defaults will be used.
>
> To produce a custom reference.odt, first get a copy of the default reference.odt: 
> 
> ```
> pandoc -o custom-reference.odt --print-default-data-file reference.odt
> ```
> 
> Then open custom-reference.odt in LibreOffice, modify the styles as you wish, and save the file.
>
> ## PowerPoint
>
> Templates included with Microsoft PowerPoint 2013 (either with `.pptx` or `.potx` extension) are known to work, as are most templates derived from these.
>
> The specific requirement is that the template should contain layouts with the following names (as seen within PowerPoint):
>
> - Title Slide
> - Title and Content
> - Section Header
> - Two Content
> - Comparison
> - Content with Caption
> - Blank
>
> For each name, the first layout found with that name will be used. If no layout is found with one of the names, pandoc will output a warning and use the layout with that name from the default reference doc instead. (How these layouts are used is described in PowerPoint layout choice.)
>
> All templates included with a recent version of MS PowerPoint will fit these criteria. (You can click on Layout under the Home menu to check.)
>
> You can also modify the default `reference.pptx`: first run 
>
> ```
> pandoc -o custom-reference.pptx --print-default-data-file reference.pptx
> ``` 
>
> and then modify `custom-reference.pptx` in MS PowerPoint (pandoc will use the layouts with the names listed above).
