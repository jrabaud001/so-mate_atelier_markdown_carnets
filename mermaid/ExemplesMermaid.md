---
title: "Exemples Mermaid.js"
author: Julien Rabaud
date: 2022-02-17
---

[Site officiel](https://mermaid-js.github.io/mermaid/#/)

## Graph

### Top-Down *Basic*

```mermaid
graph TD;  
 A-->B;  
 A-->C;  
 B-->D;  
 C-->D;
```

### Left-Right | Markdown-Pandoc

```mermaid
graph LR

  subgraph écriture
  id1(Markdown)
  end

	subgraph références-Zotero
	id7>.bib, .json, .yaml...]
	end

	subgraph Conversion
  id2((Pandoc))
  end
	
	subgraph édition
  id3(.docx)
	id3b(.odt)
  id4(.tex)
  end

	subgraph Publication
	id5(.html)
  id6(.pdf)
	id9(.epub)
  end

	id8{{+ .css, ...}}
   
  id1-->id2
  id7-->id2
  id2-->id3 & id3b & id4 & id8
  id8 -->id5 & id9
	id4-->id6

%%lien vers le manuel Pandoc
	click id2 "pandoc.org/MANUAL.html" _blank

%%lien interne vers une note dans obsidian
	class id1 internal-link

  style écriture fill:lightgreen
	style Conversion fill:lightpink
	style édition fill:lightblue
	style Publication fill:gold
	style références-Zotero fill:burlywood

	style id2 color:blue,stroke:deeppink,stroke-width:6px
	style id1 stroke:seagreen,stroke-width:6px
	style id7 stroke:crimson,stroke-width:3px
	style id8 stroke:darkturquoise,stroke-width:3px
	
	classDef ed stroke:cornflowerblue,stroke-width:3px
	class id3,id3b,id4 ed

	classDef pub stroke:darkorange,stroke-width:3px
	class id5,id6,id9 pub
```

## PieChart

```mermaid
pie title Titre
  "Calcium" : 42.96
  "Potassium" : 50.05
  "Magnesium" : 10.01
  "Iron" : 24.02
```

## Graph 2 (+styles)

```mermaid
graph LR

%% Top Node
id1(Incoming Media)

%% Media Sources
id1.1[Feedly]
id1.2{YouTube}
id1.3((Pocket Casts))
id1.4>Google Play Books]
id1.5{{General Research}}

%% Types Of Media
id2.1[/Web Articles/]
id2.2[\Books\]
id2.3[\Videos/]
id2.4[/Courses\]
id2.5(Podcasts)

id1 --> id1.1 & id1.2 & id1.3 & id1.4 & id1.5

id1.1 --> id2.1
id1.2 --> id2.3
id1.3 --> id2.5
id1.4 --> id2.4 & id2.2
id1.5 --> id2.1 & id2.4

%% Styler un élément
style id1 color:blue,fill:lightpink,stroke:deeppink,stroke-width:6px

%% Déclarer une classe
classDef niveau2 color:white,fill:royalblue,stroke:deeppink,stroke-width:3px
class id2.1,id2.2,id2.3,id2.4,id2.5 niveau2
```

## Entités - Relations

```mermaid
erDiagram  
 CUSTOMER ||--o{ ORDER : places  
 ORDER ||--|{ LINE-ITEM : contains  
 CUSTOMER }|..|{ DELIVERY-ADDRESS : uses
```

## GANTT

```mermaid
gantt
    title Refonte de l'interface de HAL
    dateFormat YYYY-MM-DD
    axisFormat %b %Y
    
	todayMarker stroke-width:5px,stroke:#0f0,opacity:0.3
    
	section Portail
    css portail :2022-04-21, 2022-05-31
    
    section Collections
    adaptées :2022-06-01, 30d
    standards :2022-09-01, 30d 
```
