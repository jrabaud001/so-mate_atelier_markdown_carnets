---
Title: Ressources libres en LLSH
author: 
    - Christel Barbisan
    - Agnès Lavoine
---

- Mindmap (jpg)
    - ![Mindmap](RL-LLSH.png)
    - [Lien sur Mindomo](https://www.mindomo.com/fr/mindmap/ressources-llsh-1e32e7e379474f4980d505deef76328b)


# Ressources libres en LLSH

## Open Access
- [Qu'est-ce que c'est ?](https://openaccess.couperin.org/category/open-access/) (Couperin)
- [Répertoire des sources en open access](http://v2.sherpa.ac.uk/opendoar/) (DOAR)

## Littérature Universitaire
- [Opengrey](http://www.opengrey.eu/)
    - Base de Littérature Grise en Europe 
    - 
      Propose un catalogue de rapport technique ou de recherche, de thèse de doctorat, d'acte de congrès, de publication officielle, de preprint de conférence...

      Base pluridisciplinaire
- [Dart-Europe E-theses](https://www.dart-europe.org/basic-search.php)
    - DART-Europe permet un accès aux thèses de recherche européennes.
- [Thèses.fr](http://www.theses.fr/)
    - "theses.fr" est un moteur de recherche des thèses de doctorat françaises (thèses de doctorat en cours de préparation en France,

      thèses de doctorat soutenues en France, tous supports)
- [HAL archives ouvertes](https://hal.archives-ouvertes.fr/)
    - Archive ouverte pluridisciplinaire, destinée au dépôt et à la diffusion d'articles scientifiques de niveau recherche, publiés ou non, et de thèses, émanant des établissements d'enseignement et de recherche français ou étrangers, des laboratoires publics ou privés.
    - [HAL-SHS](https://halshs.archives-ouvertes.fr/)
         - L'archive ouverte **HAL-SHS (Sciences de l’Homme et de la Société)**, est destinée au dépôt et à la diffusion d'articles scientifiques de niveau recherche, publiés ou non, émanant des établissements d'enseignement et de recherche français ou étrangers, dans toutes les disciplines des sciences humaines et de la société.
    - [DUMAS](https://dumas.ccsd.cnrs.fr/)
        - DUMAS, Dépôt Universitaire de Mémoires Après Soutenance, est un portail d'archives ouvertes de travaux d'étudiants à partir de bac+4 validés par un jury, dans toutes les disciplines.
- [NDLTD](http://search.ndltd.org/index.php)
    - NDLTD, Networked Digital Library of Theses and Dissertations, est un portail d'archives ouvertes de thèses et mémoires électroniques, collectés auprès d'universités du monde entier.

      Portail pluridisciplinaire.


## Moteurs de recherche spécialisés
- [Isidore](http://www.rechercheisidore.fr/)
    - Accès unifié aux données numériques de la recherche en SHS produites en France ou dans le monde francophone.
- [BASE](http://base-search.net/)
    - Moteur qui cherche dans le plus grand nombre de réservoirs de documents académiques en libre accès.
- [DOAJ](http://doaj.org/)
    - Directory of Open Access Journals : plateforme de revues scientifiques et universitaires en texte intégral validées, accessibles gratuitement, couvrant toutes les disciplines et toutes les langues
- [Dialnet](http://dialnet.unirioja.es/)
    - Accès aux références des documents scientifiques de langue espagnole. Lien vers le texte intégral des ressources quand c'est possible.
- [SciELO](http://www.scielo.org/php/index.php?lang=es)
    - Scientific electronic library online : accès à un grand nombre d'archives luso-hispaniques

## Livres numériques
- [Europeana](http://www.europeana.eu/portal)
    - Accès à + de 50 millions de documents numérisés provenant d'institutions culturelles européennes. Vérifier la licence.
- [Gallica](http://gallica.bnf.fr/)
    - Accès aux documents numérisés par la Bibliothèque nationale de France. Réutilisation libre des contenus si non commerciale.
- [DPLA](http://dp.la/)
    - Collections de nombreuses institutions culturelles nord-américaines.
- [Hathitrust](http://www.hathitrust.org/)
    - Moteur pour interroger les ouvrages numérisés consultables sur Google Books
- [BDPI](http://www.iberoamericadigital.net/es/Inicio/)
    - Accès aux collections numérisées d'institutions culturelles espagnoles et portugaises
- [InternetArchive](https://archive.org/)
    - Projet d'archivage du web. Mise à disposition de nombreux ouvrages, documents audio et vidéo

## Bases de données gratuites
- [OpenEdition](http://www.openedition.org/)
    - OpenEdition est un portail de ressources électroniques en sciences humaines et sociales. Une partie de la base est payante.
- [Persée](http://www.persee.fr/web/guest/home)
    - Persee.fr offre un accès libre et gratuit à des publications scientifiques (revues, livres, actes de colloques, publications en série, sources primaires, etc.), dans le domaine des sciences humaines et sociales mais aussi des sciences de la Terre et de l'environnement
- [Érudit](http://www.erudit.org/)
    - Érudit offre un accès libre à des revues savantes et culturelles, des livres, des actes, des mémoires et des thèses... en sciences humaines et sociales. Ressources francophones d’Amérique du Nord

## Les images
- [Images d'art](http://art.rmngp.fr/fr)
    - Oeuvres d'art sélectionnées par les musées français et regroupés par la Réunion des Musées nationaux. Télechargements possibles si usage non commercial.
- [National Gallery](https://www.nga.gov/)
- [Getty images](http://search.getty.edu/gateway/landing)
- [Creative Commons](https://search.creativecommons.org/)
- [Smithsonian Open Access](https://www.si.edu/openaccess)

## En plus
- [Canal U](http://www.canal-u.tv/)
- [Creative commons](https://creativecommons.org/)
