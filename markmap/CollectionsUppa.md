## Portail 

- <https://hal-univ-pau.archives-ouvertes.fr>
- **NG** : <https://univ-pau-pays-adour.halng.archives-ouvertes.fr/>

## Collections non-gérées par l'uppa

### EXPERICE

- <https://hal-univ-paris13.archives-ouvertes.fr/EXPERICE>

### LAM_UMR5115

- <https://halshs.archives-ouvertes.fr/LAM_UMR5115/>

## Collections UPPA personnalisées

### `BLOT`

- <https://hal-univ-pau.archives-ouvertes.fr/BLOT>
- **NG** : <https://univ-pau-pays-adour.halng.archives-ouvertes.fr/BLOT>
- SCD

### `TREE-WP`

- <https://hal-univ-pau.archives-ouvertes.fr/TREE-WP>
- **NG** : <https://univ-pau-pays-adour.halng.archives-ouvertes.fr/TREE-WP>
- Fabien Candau
- Gaëlle Deletraz

### `ITEM`

- <https://hal-univ-pau.archives-ouvertes.fr/ITEM>
- **NG** : <https://univ-pau-pays-adour.halng.archives-ouvertes.fr/ITEM>
- 

### `IRAA`

- <https://hal-univ-pau.archives-ouvertes.fr/IRAA>
- **NG** : <https://univ-pau-pays-adour.halng.archives-ouvertes.fr/IRAA>
- 

### `IPREM`

- <https://hal-univ-pau.archives-ouvertes.fr/IPREM>
- **NG** : <https://univ-pau-pays-adour.halng.archives-ouvertes.fr/IPREM>
- Sylvie Blanc <!-- \[05 59 40 78 53\] --> <sylvie.blanc@univ-pau.fr>

### `LMA-PAU`

- <https://hal-univ-pau.archives-ouvertes.fr/LMA-PAU>
- **NG** : <https://univ-pau-pays-adour.halng.archives-ouvertes.fr/LMA-PAU>
- 

## Collections UPPA standards

### STEE

#### ECOBIOP

- https://hal-univ-pau.archives-ouvertes.fr/ECOBIOP

#### LATEP

- https://hal-univ-pau.archives-ouvertes.fr/LATEP

#### LFCR

- https://hal-univ-pau.archives-ouvertes.fr/LFCR

#### SIAME

- https://hal-univ-pau.archives-ouvertes.fr/SIAME

#### LIUPPA

- https://hal-univ-pau.archives-ouvertes.fr/LIUPPA

#### NUMEA

- https://hal-univ-pau.archives-ouvertes.fr/NUMEA

#### DMEX

- https://hal-univ-pau.archives-ouvertes.fr/DMEX

### EEI

#### CDRE

- https://hal-univ-pau.archives-ouvertes.fr/CDRE

#### CREG-EA4580 \[Lirem\]

- https://hal-univ-pau.archives-ouvertes.fr/CREG-EA4580

#### IKER

- https://hal-univ-pau.archives-ouvertes.fr/IKER

### SSH

#### ALTER

- https://hal-univ-pau.archives-ouvertes.fr/ALTER

#### CATT

- https://hal-univ-pau.archives-ouvertes.fr/CATT

#### CRAJ

- https://hal-univ-pau.archives-ouvertes.fr/CRAJ

#### IE2IA

- https://hal-univ-pau.archives-ouvertes.fr/IE2IA

#### IFTJ

- https://hal-univ-pau.archives-ouvertes.fr/IFTJ

#### MEPS

- https://hal-univ-pau.archives-ouvertes.fr/MEPS

#### PDP

- https://hal-univ-pau.archives-ouvertes.fr/PDP

#### UMR6031TREE

- https://hal-univ-pau.archives-ouvertes.fr/UMR6031TREE