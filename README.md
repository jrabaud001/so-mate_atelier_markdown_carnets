# SO-MATE_atelier_markdown_carnets

> Fichiers et ressources pour l'atelier des journées annuelles SO-MATE 2022.


***

## Dossier `Stylo`

> Fichiers pour le test de l'éditeur [Stylo].
> 
> Texte de l'exemple : [Stylo : un éditeur de texte pour les sciences humaines et sociales](http://blog.sens-public.org/marcellovitalirosati/stylo/) / Vitali-Rosati,	Marcello, 2018.

- le fichier `BilletStylo.txt` contient le texte à copier-coller dans **[Stylo]** pour débuter l'exercice.
- le fichier `README.md` contient les instructions pour le *markdown-ifier*.
- le fichier `BilletStylo.md` contient le texte *markdown-ifié* avec les références biblio (pour pouvoir tester les exports).
- le fichier `Billet-Stylo.bib` contient les références bibliographiques du texte au format *BibTeX*.
  - c'est un export de la sous-collection `Billet-Stylo` du groupe Zotero SO-MATE : 
  
    `https://www.zotero.org/groups/4689886/so-mate/collections/X5C4UYM9`
  
  - ([Accéder à cette sous collection sur Zotero.org](https://www.zotero.org/groups/4689886/so-mate/collections/X5C4UYM9)).

[Stylo]: https://stylo.huma-num.fr

## Dossier `Projet`

- le fichier `readme.md` contient les commandes qui seront éxécutées pendant l'atelier.

### Sous-dossier `Article`

> Article de l'exemple : [Markdown comme condition d’une norme de l’écriture numérique](http://www.reel-virtuel.com/numeros/numero6/sentinelles/markdown-condition-ecriture-numerique) / Antoine Fauchié. *Réel Virtuel* n°6, 2017.

> Exemple d'organisation des fichiers pour la rédaction d'un article en markdown et son export vers différents formats.
- Dans le sous-dossier `data`, des fichiers pour les exports : 
    - Un *template* `.html` (modèle de document)
    - Plusieurs feuilles de styles CSS (export html)
    - Plusieurs styles bibliographiques `.csl` ([dépôt csl])
    - deux fichiers avec les mêmes références bibliographiques (`.bib` et `.json`)
    - un modèle word (`.docx`) et un modèle `.odt`
- Dans le sous-dossier `img`, les fichiers des images insérées dans l'article

[dépôt csl]: https://github.com/citation-style-language/styles



## Dossier `Modèles_Pandoc`

- sous-dossier `templates-pandoc-v2_17_1_1` : 
  - Copies des *templates* et des modèles de document par défaut de ***Pandoc***.
  - Extraits du *Manuel Pandoc* à propos des *templates* et des *modèles de documents*
- sous-dossier `various-templates`
  - des templates glanées sur les internets
- sous-dossier `mes-templates`
  - un dossier où ranger vos templates modifiées

### 


## Dossier `Présentation`

- le fichier `déroulé.md` : le diaporama écrit en markdown pour l'extension *VS Code Reveal*.
- le dossier `export` : l'export *html* du diaporama (lancer le fichier `index.html` dans un navigateur).


## Dossier `mermaid`

> Des exemples de diagrammes "en mode texte"

## Dossier `markmap`

Un fichier markdown exporté en mindmap html par le [plugin Markmap pour VS Code](https://github.com/gera2ld/markmap-vscode.git) (qui implémente la bibliothèque javascript [Markmap](https://markmap.js.org))

Un plugin pour *Obsidian* existe aussi.


## Remerciements

Je cite beaucoup de monde mais les inspirations principales pour cet atelier ont été :

- Bernard Pochet - [Markdown & vous](https://infolit.be/md/).
- Arthur Perret - Urfist de Bordeaux -[L’écriture académique au format texte : introduction](https://sygefor.reseau-urfist.fr/#/training/9139/10574).
- Damien Belvèze - *in* Stretching Numérique 2022 : [Le markdown comme syntaxe universelle](https://zenodo.org/record/6344346).
- Antoine Fauchié - [Débugue tes humanités](https://debugue.ecrituresnumeriques.ca/).
- Programming Historian :
  - [Débuter avec Markdown](https://programminghistorian.org/fr/lecons/debuter-avec-markdown)
  - [Rédaction durable avec Pandoc et Markdown](https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown)
  - [Introduction aux carnets Jupyter](https://programminghistorian.org/fr/lecons/introduction-aux-carnets-jupyter-notebooks)
- Toutes les collègues qui partagent leurs contenus.