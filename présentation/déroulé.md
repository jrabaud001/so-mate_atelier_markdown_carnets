---
title: "SO-MATE2022 - Atelier _En mode texte_ - Déroulé"
author: Julien Rabaud
date: 2022-05-19
theme: "solarized"
transition: "slide"
highlightTheme: "Routeros"
logoImg: "img/so-mate.png"
slideNumber: true
center: true
pdfSeparateFragments: false
previewLinks: true
preloadIframes: true
---

<style type="text/css">

h1, h2, h3, h4, h5, h6 {
    font-family: "maiandra gd" !important;
    /* font-weight: 600 !important;
    letter-spacing:-1px !important; */
    text-transform: none !important;
    color: Brown !important;
}

em {
    color: black !important;
}

strong {
  color: black !important;
}

section li {
  list-style-type: "ᴥ ";
}

section li li {
  list-style-type: "››› ";
}

code {
  color: Indigo;
  font-weight: 600;
}

#logo {
  height: 5rem !important;
}

.discl {
  font-size: 0.6em;
  color: hotpink;
  border: thick double hotpink;
  margin-right:2em;
  margin-top:1em;
  padding: 0.5em;
}

.sources {
  left:0;
  right:0;

  margin-left: auto;
  margin-right: auto;
  margin-top: 3em;
  position: absolute;
  width: fit-content;
  display:block;
  
  text-align:center;
  
  
  font-size: 0.5em;
  font-family: monospace;
  color: deeppink;
  border: thick double pink;
  padding: 0.5em;
}

.titrePartie {
  color: Peru !important;
}

.smaller {
  font-size:0.8em;
}

.smallest {
  font-size:0.7em;
}

</style>

***
#### SO-MATE 2022 -- Pau {.titrePartie}
***
## Atelier 
## `En mode texte`
#### Markdown - Stylo - Pandoc - Notebooks - ...
***
##### Gaëlle Deletraz (IR - UMR TREE) / Julien Rabaud (Bibas - SCD UPPA) {.smallest .titrePartie}
***
--

#### Liens à retenir pour *apprendre Markdown*

- [Tutoriel interactif multilingue](https://markdowntutorial.com)

- [Section Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown) du *Manuel Pandoc*
- [Markdown Guide](https://markdownguide.org)<span style="color:hotpink">(\*)</span>. Contient :
  - [Markdown Cheat Sheet](https://markdownguide.org/cheat-sheet) {.smaller}
    - [Basic Syntax](https://markdownguide.org/basic-syntax) {.smaller}
    - [Extended Syntax](https://markdownguide.org/extended-syntax) {.smaller}
  - \[[Tools](https://markdownguide.org/tools) - Liste de logiciels qui utilisent *markdown*.\] {.smaller}
<!-- -->

(\*) Ne traite pas des références bibliographiques, spécificité de **Pandoc**.  
Voir [Manuel Pandoc \> Citations](https://pandoc.org/MANUAL.html#citations) {style=font-size:0.6em;color:hotpink}

--

#### Fichiers binaires, fichiers textes

***

- **Fichiers binaires** : nécessitent un logiciel spécifique (souvent avec license) pour être lus et édités : {style=font-size:0.8em;}
  - `.docx`, `.odt`, `.pdf`

***

- **Fichiers textes** : lisibles et modifiables dans un simple éditeur de texte : {style=font-size:0.8em;}
  - `.md`, `.html`, `.tex`, `.csv`, `.json`, `.bib`, `.css`, `.py`, ...

***

- **\+** peuvent être manipulés par des **utilitaires standards** du monde libre, très puissants : {.fragment .fade-left style=font-size:0.8em;}
    - `awk`, `sed`, `grep`, `git`, `bash`, ...

***

<aside class="notes">

dézipper un document word

dézipper un document epub

</aside>

--

> **Markdown** est un langage de balisage léger créé en 2004 par John Gruber avec l'aide d'Aaron Swartz. Il a été créé dans le but d'offrir **une syntaxe facile à lire et à écrire**.  
> Un document balisé par Markdown peut être lu en l'état sans donner l’impression d'avoir été balisé ou formaté par des instructions particulières. {style=background-color:mistyrose;color:teal;text-align:justify;font-size:0.66em;padding:.5em}

source : Wikipedia (fr) -- [Markdown](https://en.wikipedia.org/wiki/Markdown) {style=font-size:0.5em;text-align:right;margin-right:130px;}

--

***

Fond - Structuration - Balisage

***

Forme - Composition - Feuille de style

***

<span class="discl">/!\ Je ne suis pas sémiologue</span> {.fragment .fade-right}


---

## <span class="titrePartie">Ma rencontre avec *Markdown*</span>

---

### Des logiciels qui l'implémentent

--

- *Framanotes* (fermé)
  - (Instance de [Turtle](https://github.com/turtl) - alternative à *Pocket*)
- -> ***Zotermite*** (R. de Mourat) - \[[sources](https://github.com/robindemourat/zotermite)\]
- Les notes dans *[Vivaldi](https://vivaldi.com/fr/)* (navigateur web, non-libre), 
 
- La zone de transcription dans *[Tropy]*, 
- Les fichiers `README.md` des dépôts *Github*...

[Tropy]: https://docs.tropy.org/in-the-item-view/notes

--

### Aujourd'hui encore plus 

Exemples récents : {style=text-align:left}
- le nouveau gestionnaire de notes de *Zotero*
  - [Zotero 6 : transformer votre flux de travail de recherche](https://zotero.hypotheses.org/4145){style=font-size:0.8em}
- *GoogleDocs* en saisie
  - [Utiliser Markdown dans Google Docs, Slides et Drawings](https://support.google.com/docs/answer/12014036?hl=fr){style=font-size:0.8em} 
- conversations dans *Microsoft Teams*
  - [Utiliser la mise en forme Markdown dans Teams](https://support.microsoft.com/fr-fr/office/utiliser-la-mise-en-forme-markdown-dans-teams-4d10bd65-55e2-4b2d-a1f3-2bebdcd2c772){style=font-size:0.8em}


---

### Des collègues qui l'utilisent

--

#### Frédérique Flamerie de la Chapelle

*Open access and data librarian* - Université de Bordeaux {.smaller}

- 2017 - [atelier Zotero-CSL](https://github.com/fflamerie/zotero_csl_2017)
- 2019 - [formations *Open access and beyond* et *How to build your online researcher profile ?* pour les ED de l'UPPA](https://github.com/fflamerie/pau_ed_2019) 

--

#### Cécile Arènes

*Conservatrice des bibliothèques*  
Chargée de mission Données de la recherche et Humanités numériques à la bibliothèque de *Sorbonne Université*. {.smaller}

- 2021 - Urfist Bordeaux - [Formation Plan de gestion de données](https://github.com/carenes/urfist_bdx_DMP) (sources en *Rmarkdown*, export présentation `.html` et `.pdf`).

- [Un dépôt Github avec des *templates* de PGD](https://github.com/carenes/DMP_templates) (H2020 et ANR) aux formats *TeX* et *Rmarkdown*.
---

### Des chercheurs en design et en SIC

--

#### Robin de Mourat
Designer de recherche en méthodes numériques  
*Medialab* -- Science-Po {.smaller}

- *Projet* [**Peritext**. Écrire et éditer avec les matériaux de la recherche](https://peritext.github.io/fr/) (Médialab). {.smaller}
  - *Livrables* : [Ovide](https://medialab.sciencespo.fr/outils/ovide/) \& [Fonio](https://medialab.sciencespo.fr/outils/fonio/). {.smaller}
- Une *conférence* \[vidéo - 1h16\]: [Des formats de recherche aux formats de publication](https://www.canal-u.tv/chaines/esad-grenoble-valence/penser-un-espace-editorial/robin-de-mourat-des-formats-de-recherche), (ÉSAD Grenoble - Valence, 2018). {.fragment .fade-left .smaller}
  - L'*article* associé : [Le design fantomatique des communautés savantes : enjeux phénoménologiques, sociaux et politiques de trois formats de données en usage dans l’édition scientifique contemporaine](https://www.cairn.info/revue-sciences-du-design-2018-2-page-34.htm), *Sciences du Design* n° 8, déc. 2018, p. 34‑44. \[*Un des trois format analysé est markdown*\]  {.smaller}
- Sa *thèse* : [Le Vacillement des formats. Matérialité, écriture et enquête : le design des publications en Sciences Humaines et Sociales](http://www.these.robindemourat.com/) (*Esthétique et Arts appliqués*, Rennes, 2020). {.fragment .fade-left .smaller}

--

#### Antoine Fauchié

Doctorant -- Chaire de recherche du Canada en *Écritures numériques*. {.smaller}

- Musée Saint-Raymond (Toulouse) : [Un catalogue numérique \[et papier\]](https://saintraymond.toulouse.fr/Le-catalogue-numerique-des-sculptures-de-Chiragan_a1192.html) (avec [Julie Blanc](https://julie-blanc.fr/projects/villa-chiragan/)) {.smaller}
- Une série de billets sur son blog : ["Les fabriques de publication"](https://www.quaternum.net/2020/04/29/les-fabriques-de-publication/) {.fragment .smaller}
  - | [LaTeX](https://www.quaternum.net/2020/04/29/fabriques-de-publication-latex/) | [Pandoc](https://www.quaternum.net/2020/04/30/fabriques-de-publication-pandoc/) | [Asciidoctor](https://www.quaternum.net/2020/05/03/fabriques-de-publication-asciidoctor/) | [Jekyll](https://www.quaternum.net/2020/05/05/fabriques-de-publication-jekyll/) | [Org-Mode](https://www.quaternum.net/2020/05/10/fabriques-de-publication-org-mode/)| [Quire](https://www.quaternum.net/2020/05/13/fabriques-de-publication-quire/) | [Gabarit Abrupt](https://www.quaternum.net/2020/05/18/fabriques-de-publication-gabarit-abrupt/) | [Stylo](https://www.quaternum.net/2020/05/21/fabriques-de-publication-stylo/) | [Zettlr](https://www.quaternum.net/2020/08/28/fabriques-de-publication-zettlr/) | {.smallest}
- Deux articles : {.fragment .smaller}
  - [Markdown comme condition d’une norme de l’écriture numérique](http://www.reel-virtuel.com/numeros/numero6/sentinelles/markdown-condition-ecriture-numerique), *Réél - Virtuel*, nᵒ 6, 2018. {.smaller}
  - [Les technologies d’édition numérique sont-elles des documents comme les autres ?](https://publications-prairial.fr/balisages/index.php?id=321) , *Balisages*, nᵒ 1, févr. 2020, `doi: 10.35562/balisages.321`. {.smaller}
- Son mémoire de master (2019) : [Vers un système modulaire de publication : éditer avec le numérique](https://memoire.quaternum.net/) {.fragment .smaller}
- Son TD 2021-2022 : [Débugue tes humanités](https://debugue.ecrituresnumeriques.ca/)
(20 séances filmées) {.fragment .smaller}

--

#### Arthur Perret

Doctorant -- *Laboratoire MICA*, UBM. {style=font-size:0.8em}

- Tout [son blog](https://www.arthurperret.fr) mais en particulier : {.smaller}
  - 1^er^ billet : [Une micro-chaîne éditoriale multicanal pour l’écriture scientifique](https://www.arthurperret.fr/blog/2018-10-18-une-micro-chaine-editoriale.html) (2018-10-18) {.smaller}
  - [Dr Pandoc & Mr Make](https://www.arthurperret.fr/blog/2020-09-14-dr-pandoc-and-mr-make.html) (2020-09-14) {.smaller}
  - [Du notebook au bloc-code](https://www.arthurperret.fr/blog/2021-06-11-du-notebook-au-bloc-code.html) (2021-06-11) {.smaller}
  - [Cosma, de la fiche au graphe](https://www.arthurperret.fr/blog/2021-09-04-cosma-de-la-fiche-au-graphe.html) (2021-09-04) \[[Cosma, lien direct](https://cosma.graphlab.fr/)\] {.smaller}
  - [Analyser, synthétiser, visualiser : le triptyque fiche, lien, graphe](https://www.arthurperret.fr/blog/2022-02-17-analyser-synthetiser-visualiser.html) (2022-02-17) {.smaller}

- Un article {.smaller}
  - Perret, Arthur et Le Deuff, Olivier (2020). « All papers are data papers: from open principles to digital methods ». [_DH2020 Book of Abstracts_](https://dh2020.adho.org/wp-content/uploads/2020/07/582_Allpapersaredatapapersfromopenprinciplestodigitalmethods.html). {.smaller}

<span class="discl">la rubrique [veille]() de son blog est une mine aussi !<span> {.fragment}

---

## <span class="titrePartie">Mes expériences</span>  
## <span class="titrePartie">"en mode texte"</span>

--

### L'inventaire Jacques Blot

- [Documentation du projet](https://inventaire-blot.netlify.app/) (site perso)

  - [pdf de la présentation](https://hal-univ-pau.archives-ouvertes.fr/hal-03025301/file/Presentation-BLOT_omekaNancy13102020%20%282%29.pdf) :
  
  Gaelle Chancerel, Julien Rabaud. *Bibliothèque Omeka Jacques Blot : Valorisation d’un fonds patrimonial des monuments protohistoriques basques*. Omeka — Projets scientifiques, culturels et/ou documentaires, Octobre 2020, Nancy. `⟨hal-03025301⟩` {.smaller}

- [Collection OmekaS](https://kumbuka.univ-pau.fr/s/Collection_Blot/page/welcome) {.smaller}
  
  - Les medias **html** des items monuments ont été généré avec Pandoc. 


--

### Mémoire de master en littérature

Accompagnement d'un étudiant. Écriture avec [Zettlr](https://www.zettlr.com/) (et Zotero).<br>
Exports **.docx** (avec modèle), **html** (template, css, javascript) et **pdf** (template). {.smallest}

  - *Sources* ([Gitlab UPPA](https://git.univ-pau.fr/jrabaud001/memoire-djeban-landry-koffi))
    - [Version **html**](https://master2-kristof.netlify.app)
    - [Version **pdf**](https://master2-kristof.netlify.app/data/memoire-djeban-landry-koffi.pdf)

--

### Des diaporamas 

#### `markdown -> reveal.js`

- Atelier BU - [Prendre des notes et rédiger en Markdown](https://atelier-markdown-uppa.netlify.app/). {style=font-size:0.8em}

- [Introduction à la Science Ouverte](https://intro-so-doct-uppa.netlify.app/), Journées IST 2021-2022, Écoles doctorales - SCD UPPA, décembre 2021. {style=font-size:0.8em}

--

### Un blog personnel avec [Eleventy](https://www.11ty.dev/)

*Générateur de site statique*  
(comme [Jekyll](https://jekyllrb.com), [Hugo](https://gohugo.io) ou [Gatsby](https://gatsbyjs.com)...) {.smaller}

***

[Carnet de UjuBib](https://uju-11ty.netlify.app/) 

***

<span class="discl">/!\ pas actualisé depuis un an !</span>


--

### Prises de notes avec *Obsidian*

Depuis deux ans.

- démo -->

- [Mindmap *LiensObsidian*](https://mindmap-obsidian.netlify.app/)

--

### Auto-hébergement d'une instance *HedgeDoc*

- sur la plateforme *Heroku* ([how to](https://docs.hedgedoc.org/setup/heroku/))

  - [~~lien de mon instance~~](https://uju-hedgedoc.herokuapp.com) [fermée] {.smaller}
  - [Documentation HedgeDoc](https://docs.hedgedoc.org/) {.smaller}

--

#### Découverte des Notebooks

- [Python pour la science ouverte](https://github.com/ml4rrieu/py_so) (formation de [Maxence Larrieu](http://maxence.larri.eu) à l'Urfist de Toulouse).
  - Carnets *Jupyter* Autour de l'API de HAL {.smaller}

- Carnets qui deviennent un [format de soumission]() pour des *revues de shs*.

- *Observable* via [Antoine Courtin](https://antoinecourtin.github.io/markdown-cv/) (à l'époque à l'INHA).

- [Rzine](https://rzine.fr) 
  - Pour la diffusion et le partage de ressources sur la pratique de R en sciences humaines et sociales" \[*en Rmarkdown*\]


---

## <span class="titrePartie">*Markdown* en pratique</span>

<span class="sources">Sources de la présentation et fichiers pour les exercices --> [Gitlab UPPA](https://git.univ-pau.fr/jrabaud001/so-mate_atelier_markdown_carnets)</span>

---

### Écrire en markdown dans *Stylo*

- Aller dans le sous-dossier `Stylo` du dossier Gitlab.
- Copier-coller dans *Stylo* le contenu du fichier texte.txt

Un autre éditeur en ligne d'articles scientifiques,<br> collaboratif et basé sur *markdown* : [Authorea](https://www.authorea.com/) {.smallest}

--

### Les citations avec *Better BibTeX for Zotero*
https://retorque.re/zotero-better-bibtex/

--> démo

---

## Exercices *Pandoc* 

--

### Conversion de documents avec *Pandoc*

- Se placer dans le dossier `Projets/Article`

- Ouvrir `article.md`

--

#### Modèles de documents et *templates*

***

- [Guide Pandoc : Templates](https://pandoc.org/MANUAL.html#templates)

--

##### Démo `docx`

***

- [Guide Pandoc : export docx / odt]()

--

##### Démo `html` (*css*, *templates*)

***

- [Guide Pandoc : export html]()

--

##### Démo `pdf` (Paramètres LaTeX, *templates*)

***

- [Guide Pandoc : export pdf]()

---

## `YAML`

<div style="font-size:0.5em;margin-top:-1.5em;color:green;font-family:monospace;font-weight:600;letter-spacing:3px">YAML Ain't Markup Language<br>(Yet Another Metadata Language)</div>

### Métadonnées, options & paramètres

--

### Pour tous les *writers* 

--

### Pour Html

--

### Pour LaTeX/pdf

--

### Les *Defaults Files*

[Guide Pandoc](https://pandoc.org/MANUAL.html#defaults-files)

```sh
pandoc -d file 
```

--

### Des métadonnées sémantiques

[Metadata for markdown / MkDocs](https://blogs.pjjk.net/phil/metadata-for-markdown-mkdocs/) :  {.smallest}

>#### What does JSON-LD in YAML look like?
>
> The metadata that we use for OCX is a profile of schema.org / LRMI,  OERSchema and few bits that we have added because we couldn’t find them elsewhere. Here’s what (mostly) schema.org metadata looks like in YAML: {style=background-color:mistyrose;color:teal;text-align:justify;font-size:0.66em;padding:.5em}

--

```yaml
"@context":
    - "http://schema.org"
    - "oer": "http://oerschema.org/"
    - "ocx": "https://github.com/K12OCX/k12ocx-specs/"
"@id": "#Lesson1"
"@type":
    - oer:Lesson
    - CreativeWork
learningResourceType: LessonPlan
hasPart:
    "@id": "#activity1-1"
author:
    "@type": Person
    name: Phil Barker
```

---



## AutoPandoc

- (sur le github d'Arthur Perret) : https://github.com/infologie/autopandoc

- (cloné sur mon github) : https://github.com/ujubib/APautopandoc

---

## Notebooks

- [Le notebook et la programmation lettrée](https://github.com/HuguesPecout/notebook_mateshs) : Diaporama utilisé lors des Journées annuelles MATE-SHS 2022

- 

---

### Jupyter

Installer avec [Anaconda](https://www.anaconda.com/products/distribution) ou en ligne de commandes avec les outils python standards (`pip`).

Documentation officielle : https://docs.jupyter.org/en/latest/

--

#### Un format de soumission :  
#### *Journal for digital history*

- [*Guidelines for authors*](https://journalofdigitalhistory.org/en/guidelines) : expliquent entre autres comment paramétrer *Zotero* avec *Jupyter*.

--

#### Google Colab

https://colab.research.google.com

---

### Rmarkdown et *Quarto*


--

#### Comment R Markdown produit-il un document ?

![Comment Rmarkdown produit-il un document ?](https://www.book.utilitr.org/pics_resized/rmarkdown/schema_rmd.png)

source : https://www.book.utilitr.org/rmarkdown.html {style=font-size:0.5em;color:salmon}
--

#### Paramètres dans l'en-tête 

```yaml
--- 
title: "Produire des documents avec R Markdown"
author: "Daffy Duck"
date: "2022-04-22"
output: 
  html_document:
    keep_md: true
    self_contained: true
    highlight: kate
    toc: yes
  pdf_document: 
    fig_caption: yes
    highlight: kate
bibliography: book.bib
description: "Un document où je révèle que je ne suis pas un canard"
---
```

source : https://www.book.utilitr.org/rmarkdown.html {style=font-size:0.5em;color:salmon}

--

#### Paramètres des blocs de code R (*chunks*)

- nommage (recommandé) `{r mon_bloc}`
  ```md
  ```{r calcul1}
  x <- 2+7
  x

  ```

- permet de les appeler dans *RStudio*

--

- options d'affichage (pour le rendu par *knitr*) :  
  `{r echo=FALSE, warning=FALSE}`

  |Option     |Valeurs possibles|Valeur par défaut|
  |-----------|:---------------:|:---------------:|
  |**echo**   | `TRUE/FALSE`    | `TRUE`          |
  |**eval**   | `TRUE/FALSE`    | `TRUE`          |
  |**include**| `TRUE/FALSE`    | `TRUE`          |
  |**results**| `'hide'/'asis'/'markup'/'hold'`|`'markup'`|
  |**error**  | `TRUE/FALSE`    | `TRUE`          |
  |**warning**| `TRUE/FALSE`    | `TRUE`          |
  |**message**| `TRUE/FALSE`    | `TRUE`          | {style=font-size:0.7em}

source : https://www.book.utilitr.org/rmarkdown.html {style=font-size:0.5em;color:salmon}

--

- dimensions de la figure (si figure)

```
```{r, fig.width=12, fig.height=8}
hist(iris$Sepal.Length)
...
```

![](https://www.book.utilitr.org/DocumentationR_files/figure-html/unnamed-chunk-402-1.png){width=50%}


source : https://www.book.utilitr.org/rmarkdown.html {style=font-size:0.5em;color:salmon}


---

### Observable

- plugin d'import dans VS Code : [Observable JS](https://github.com/GordonSmith/vscode-ojs)

- [Introduction to Code / Observable](https://observablehq.com/@observablehq/introduction-to-code)

- [Introduction to HTML / Observable](https://observablehq.com/@observablehq/introduction-to-html?collection=@observablehq/introduction)

---

## Questions ?

---

## MERCI !